# NETCONF Server

This project implements a Flask-based server that simulates a NETCONF interface to configure Time-Aware Priority (TAPRIO) scheduling on network devices. It processes incoming XML configuration data, extracts TAPRIO scheduling details, and applies them to the specified network device.

You must have an SSL certificate to enable HTTPS communication between the Netconf server and the [Client](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/blob/main/src/gcl_controller.py?ref_type=heads). (gcl_controller)

## Features

- Simulates NETCONF `edit-config` operation via a Flask route.
- Parses incoming XML data to extract TAPRIO configuration.
- Applies TAPRIO scheduling to network devices using extracted data.
- Supports SSL/TLS for secure communication.

## Usage

To run the server, execute the following command: ``` sudo python3 netconf_server.py```

#!/usr/bin/python
from flask import Flask, request, Response
import logging
from lxml import etree
import os
import ssl

# Initialize Flask App
app = Flask(__name__)

# Initialize Logger
logger = logging.getLogger(__name__)

# Global variables
SERVER_DEBUG = False

NS = {
    "nc": "urn:ietf:params:xml:ns:netconf:base:1.0",
    "taprio": "urn:ietf:params:xml:ns:yang:taprio-schedules",
}

TAPRIO_OP = {
    "{urn:ietf:params:xml:ns:yang:taprio-schedules}device": "dev",
    "{urn:ietf:params:xml:ns:yang:taprio-schedules}command": "cmd",
    "{urn:ietf:params:xml:ns:yang:taprio-schedules}gatemask": "mask",
    "{urn:ietf:params:xml:ns:yang:taprio-schedules}interval": "interval"
}

# Yang utilities function to get TAPRIO configuration
def get_taprio_config(netconf_data):
    taprio_config = {}
    device = netconf_data.find('.//taprio:device', namespaces=NS).text
    taprio_config['device'] = device
    taprio_config['sched_entries'] = []
    
    for entry in netconf_data.findall('.//taprio:sched-entry', namespaces=NS):
        sched_entry = {
            'command': entry.find('taprio:command', namespaces=NS).text,
            'gatemask': entry.find('taprio:gatemask', namespaces=NS).text,
            'interval': entry.find('taprio:interval', namespaces=NS).text
        }
        taprio_config['sched_entries'].append(sched_entry)
    
    return taprio_config

# Utility function to apply TAPRIO configuration
def apply_taprio_config(device, sched_entries):
    cmd_base = "tc qdisc replace dev {} parent root handle 100 taprio num_tc 8 " \
               "map 0 1 2 3 4 5 6 7 1 1 1 1 1 1 1 1 " \
               "queues 1@0 1@1 1@2 1@3 1@4 1@5 1@6 1@7 base-time 100000 clockid CLOCK_TAI"
    cmd = cmd_base.format(device)

    for entry in sched_entries:
        cmd += " sched-entry S {} {} ".format(entry['gatemask'], entry['interval'])

    logger.info(f"Executing command: {cmd}")
    os.system(cmd)

# Define a route for configuration updates, simulating the edit-config RPC
@app.route('/edit-config', methods=['POST'])
def edit_config():
    raw_xml_data = request.data.decode('utf-8')  # Decode request data from bytes to string
    logger.debug(f"Raw XML received: {raw_xml_data}")

    if request.content_type == 'application/xml':
        try:
            xml_data = etree.fromstring(raw_xml_data)
            taprio_config = get_taprio_config(xml_data)
            apply_taprio_config(taprio_config['device'], taprio_config['sched_entries'])
            return Response("<ok/>", mimetype='application/xml')
        except etree.XMLSyntaxError as e:
            logger.error(f"XML syntax error: {e}")
            return Response(f"<error>XML syntax error: {e}</error>", mimetype='application/xml'), 400
        except Exception as e:
            logger.error(f"Error processing configuration: {e}")
            return Response(f"<error>General error: {e}</error>", mimetype='application/xml'), 500
    else:
        return Response("<error>Unsupported Media Type</error>", mimetype='application/xml'), 415


if __name__ == "__main__":
    # Load SSL/TLS certificates
    #ssl_cert = 'cert.pem'
    #ssl_key = 'key.pem'

    # Create SSL context
    #ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    #ssl_context.load_cert_chain(certfile=ssl_cert, keyfile=ssl_key)

    # Run Flask app with SSL/TLS enabled
    app.run(host='0.0.0.0', port=8300, debug=SERVER_DEBUG, ssl_context=('cert.pem', 'key.pem'))

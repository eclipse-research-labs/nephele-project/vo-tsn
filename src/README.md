# TSN Control Plane
This folder provides a part of the functionalities of a TSN CNC. The control plane provides a Flask server that provides a form for uploading JSON files (network_data and flow_data), and communicate with the [Scheduler](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/tree/main/schedulerSRC?ref_type=heads) to configure network device Time-Aware Priority (TAPRIO) schedules.
## Running the scripts
## Getting Started

Follow these steps to set up and run the project:

1. **Start the Netconf Server**: Ensure the [Netconf server](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/tree/main/netconfServer?ref_type=heads) is up and running. 
2. **Run the Server Script**: Start the Flask server by running the `server.py` script. Open your terminal and execute: ``` python3 server.py```
3. **Access the Web Interface**: Open a web browser and navigate to [http://localhost:5000](http://localhost:5000). This is the home page for the project's web interface.
4. **Upload Configuration Files**: Through the web interface, upload the `network_data.json` and `flow_data.json` files. These files contain essential data for configuring the network.
5. **Configure the Devices**: Once the JSON files are uploaded, use the web interface to push the configuration to the devices by clicking the 'Configure Device' button.


## Details

Details of the functionalities of each script within the `src` directory.

### `flow_and_path.py`

This script is designed to interact with a flask server (server.py) and a specified API to retrieve and process network flow data.

#### Functionality:
- **make_api_request**: Takes a URL, headers, and data to make a GET request to an API endpoint. If the request is successful, it returns the JSON response; otherwise, it prints an error message with the status code and exits the program.

- **main**: Acts as the entry point of the script. It performs the following actions:
  1. Execute a GET request to the flask server at 'http://localhost:5000/combine_data' to retrieve data needed for the API request.
  2. Reads the `flow_request.json` file, which should contain the request payload.
  3. Calls `make_api_request` to the Scheduler with the API URL 'http://localhost:8080/schedule/taprio', headers for JSON content type, and the payload from `flow_request.json`.
  4. Print the response from the API to the console, detailing each flow's ID, start time, and arrival time.

#### Error Handling:
- If either the local server request or the API request fails, the script will output an error message detailing the issue.

#### Usage:
Execute the script with no arguments. Ensure that the server.py is running and accessible, and that `flow_request.json` is present and formatted correctly.


### `gcl_controller.py`

This script is essential for configuring Time-Aware Priority (TAPRIO) schedules on network devices using the NETCONF protocol over HTTPS.

#### Functionality:

- **configure_taprio_schedules**: Sends an XML configuration to the network device to set TAPRIO schedules. It builds an XML payload according to NETCONF standards and posts it to the device using HTTPS. The function logs the status and prints the configuration to be sent. It also handles any errors in communication with the device.

- **get_taprio_config**: Parses the NETCONF XML data to extract TAPRIO configuration details like the device name and schedule entries.

- **main section**: Orchestrates the script's flow by:
  1. Invoking `flow_and_path_main` from `flow_and_path.py` to get the necessary data for TAPRIO configuration.
  2. Looping through the switches and their interfaces as provided by the API response to configure the TAPRIO schedules.
  3. Measuring the execution time of the configuration commands for performance analysis.
  4. Calculating and logging the average execution time after conducting a specified number of experiments (configurable via `_N_EXPERIMENT`).

#### Hardcoded Values:

- `_SERVER_IP`: The IP address of the NETCONF server.
- `_DEVICE`: The network interface name to be configured.
- `_N_EXPERIMENT`: The number of times the TAPRIO schedule configuration will be sent to calculate the average execution time.

You can hide this information from the script by using a configuration file (config.ini) instead of hardcoded values

#### Logging:

The script uses Python's `logging` library for logging informational messages, errors, and average execution times.

#### Error Handling:

The script includes exception handling for errors that may occur during the HTTP request or XML parsing, with appropriate error messages being logged.

#### Usage:

Run the script to automatically configure TAPRIO schedules on the specified device. Ensure that the NETCONF server is accessible at `_SERVER_IP`, and that `cert.pem` is available for HTTPS verification. The device name and other configurations should be adjusted as per the network setup before executing the script.


### `server.py`

This script launches a Flask web server that provides a user interface and API for uploading, validating, and combining network and flow data, as well as initiating the TAPRIO schedule configuration via the `gcl_controller.py` script.
![alt text](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/raw/main/documents/upload_forms.png?ref_type=heads)


#### Features:

- **Endpoints**:
  - `/`: Serves the main index page of the web application.
  - `/upload_network_data`: Accepts a file upload, validates it against `network_schema.json`, and stores the data if it's valid.
  - `/upload_flow_data`: Accepts a file upload, validates it against `flow_schema.json`, and stores the data if it's valid.
  - `/combine_data`: Combines the valid network and flow data and writes it to `flow_request.json`.
  - `/execute_gcl_controller`: Executes the `gcl_controller.py` script and returns its output.

- **Data Validation**:
  Utilizes the `jsonschema` library to validate the uploaded JSON data against predefined schemas, ensuring the data structure is correct before processing.

- **Error Handling**:
  Each route is equipped to handle errors such as invalid data format, missing files, or failed script execution, providing clear JSON responses with error messages.

#### Usage:

1. Start the server by running the script: `python3 server.py`.
2. Navigate to `http://localhost:5000` in a web browser to access the user interface.
3. Use the provided endpoints to upload network and flow data, and to trigger TAPRIO configuration.

#### Requirements:

- Flask: for the web server and routing.
- jsonschema: for validating JSON data against schemas.
- subprocess: for running the `gcl_controller.py` script.

#### Development:

The server is configured to run in debug mode, which provides detailed error messages and auto-reloads the server on code changes. This should be disabled in a production environment for security reasons.

.

## Data Files

Descriptions of the JSON files:

### `flow_data.json`

This JSON file contains a list of flow specifications for network traffic. Each flow is defined by a set of attributes:

#### Attributes:

- **flowId**: A unique identifier for the flow.
- **deadline**: The maximum time (in microseconds) from the start of a period to the completion of the flow's transmission.
- **packetSize**: The size of packets for the flow in bytes.
- **period**: The interval (in microseconds) at which the flow repeats its transmission.
- **path**: An array representing the sequence of nodes that the flow traverses, starting from the source and ending at the destination.

#### Example Flow Entry:

```json
{
    "flowId": "flow1",
    "deadline": 800,
    "packetSize": 1500,
    "period": 800,
    "path": ["n1", "n2", "hub", "n3"]
}
```
### `network_data.json`

This JSON file contains the structure of the network topology, specifying nodes and the connections between them (edges).

#### Structure:

- **nodes**: An array of objects, each representing a network element. Each node has:
  - `id`: A unique identifier for the node.
  - `type`: The role of the node in the network, such as 'endpoint' or 'switch'.
  - `processingDelay` (if applicable): The time taken by a switch to process a packet, measured in microseconds.

- **edges**: An array of objects, each representing a link between two nodes. Each edge has:
  - `link`: A unique identifier for the link.
  - `nodes`: An array of two node identifiers that the link connects.
  - `delay`: The transmission delay of the link in microseconds.
  - `speed`: The speed of the link in megabits per second (Mbps).

#### Example Network Data Entry:

```json
{
    "network": {
        "topology": {
            "nodes": [
                {"id": "n1", "type": "endpoint"},
                {"id": "n2", "type": "switch", "processingDelay": 1},
                {"id": "hub", "type": "switch", "processingDelay": 2},
                {"id": "n3", "type": "endpoint"}
            ],
            "edges": [
                {"link": "l1", "nodes": ["n1", "n2"], "delay": 1, "speed": 100},
                {"link": "l2", "nodes": ["n2", "hub"], "delay": 1, "speed": 100},
                {"link": "l3", "nodes": ["hub", "n3"], "delay": 1, "speed": 100}
            ]
        }
    }
}
```


## Schemas

### `network_schema.json`:

This JSON schema defines the structure and rules for validating the `network_data.json` file's contents. It ensures that the network topology data adhere to the expected format before it's processed by the system.

### `flow_schema.json`: 
This JSON schema defines the structure and rules for validating the `flow_data.json` file's contents. It ensures that the flow data adhere to the expected format before it's processed by the system.






import time
import logging
import requests
from lxml import etree
from flow_and_path import main as flow_and_path_main
# Hardcoded credentials and configuration
_SERVER_IP = "https://localhost:8300"  # Note the 'https' prefix for secure communication
_DEVICE = "enp0s3"
_N_EXPERIMENT = 1

# Setup logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def configure_taprio_schedules(dev_name, cmd, schedule_list):
    """
    Configures TAPRIO schedules on a network device using HTTPS POST.
    """
    # Ensure that session is defined
    session = requests.Session()

    logger.info(f"Configuring TAPRIO schedules on {dev_name} with command '{cmd}'.")
    print(f"Configuring TAPRIO schedules on {dev_name} with command '{cmd}'.")

    config = f"""
    <rpc message-id="101"
    xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
    <edit-config>
        <target>
            <running/>
        </target>
        <default-operation>none</default-operation>
        <test-option>test-then-set</test-option>
        <error-option>rollback-on-error</error-option>
        <config xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
            <taprio-schedules operation="{cmd}" xmlns="urn:ietf:params:xml:ns:yang:taprio-schedules">
                <schedules>
                    <device>{dev_name}</device>
                    <sched-entries>"""

    for schedule in schedule_list:
        config += f"""
                        <sched-entry>
                            <command>S</command>
                            <gatemask>{schedule['gatemask']}</gatemask>
                            <interval>{schedule['interval']}</interval>
                        </sched-entry>"""

    config += """
                    </sched-entries>
                </schedules>
            </taprio-schedules>
        </config>
    </edit-config>
    </rpc>"""  # Close the rpc tag here

    print("Configuration to be sent with RPC:")
    print(config)

    try:
        response = session.post(f"{_SERVER_IP}/edit-config", data=config, headers={'Content-Type': 'application/xml'}, verify='cert.pem')
        if response.status_code == 200 and 'application/xml' in response.headers.get('Content-Type', ''):
            try:
                response_xml = etree.fromstring(response.content)
                # Process the XML response, check for errors, etc.
                # ...
            except etree.XMLSyntaxError as e:
                logger.error(f"Error parsing response XML: {e}")
        else:
            logger.error(f"Received an error from server: {response.status_code}")
    except Exception as e:
        logger.error(f"Error sending TAPRIO configuration: {e}")

def get_taprio_config(netconf_data):
    logger.info("Parsing XML data to extract TAPRIO configuration...")
    taprio_config = {}
    device_element = netconf_data.find('.//device')
    if device_element is not None:
        device = device_element.text
        logger.info(f"Device found: {device}")
        taprio_config['device'] = device
    else:
        # Handle the case when device element is not found
        logger.error("Device element not found in XML data.")
        return None  # Return None to indicate failure
    
    taprio_config['sched_entries'] = []
    
    for entry in netconf_data.findall('.//sched-entry'):
        sched_entry = {
            'command': entry.find('command').text,
            'gatemask': entry.find('gatemask').text,
            'interval': entry.find('interval').text
        }
        taprio_config['sched_entries'].append(sched_entry)
    
    logger.info("TAPRIO configuration extracted successfully.")
    return taprio_config

# Get API response from the main function in flow_and_path script
api_response = flow_and_path_main()

# Configure TAPRIO schedules and calculate average execution time
execution_times = []
try:
    for switch in api_response["switches"]:
        switch_id = switch["switch"]
        schedule_list = switch["interfaces"][0]["schedEntries"]
        for _ in range(_N_EXPERIMENT):
            start_time = time.time()
            configure_taprio_schedules(switch_id, "create", schedule_list)
            elapsed_time = time.time() - start_time
            execution_times.append(elapsed_time)
            logger.info(f"Elapsed time for iteration: {elapsed_time} seconds")
except Exception as e:
    logger.error(f"An error occurred in the loop: {e}")

# Calculate and log the average execution time
if execution_times:
    average_execution_time = sum(execution_times) / len(execution_times)
    logger.info(f"Average Execution Time: {average_execution_time}")
else:
    logger.info("No execution times to average.")

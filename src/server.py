from flask import Flask, render_template, request, jsonify, send_file
import json
import subprocess
from jsonschema import validate
from jsonschema.exceptions import ValidationError

app = Flask(__name__)

# Load JSON schemas from separate files
with open('network_schema.json', 'r') as file:
    network_schema = json.load(file)

with open('flow_schema.json', 'r') as file:
    flow_schema = json.load(file)

network_data = None
flow_data = None

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload_network_data', methods=['POST'])
def upload_network_data():
    global network_data
    network_file = request.files['network_file']
    if network_file:
        network_data = json.load(network_file)
        try:
            validate(instance=network_data, schema=network_schema)
        except ValidationError as e:
            return jsonify({'error': 'Invalid network data format', 'details': str(e)})
        return jsonify({'message': 'Network data uploaded successfully'})
    else:
        return jsonify({'error': 'No file uploaded'})

@app.route('/upload_flow_data', methods=['POST'])
def upload_flow_data():
    global flow_data
    flow_file = request.files['flow_file']
    if flow_file:
        flow_data = json.load(flow_file)
        try:
            validate(instance=flow_data, schema=flow_schema)
        except ValidationError as e:
            return jsonify({'error': 'Invalid flow data format', 'details': str(e)})
        return jsonify({'message': 'Flow data uploaded successfully'})
    else:
        return jsonify({'error': 'No file uploaded'})

@app.route('/combine_data', methods=['GET'])
def combine_data():
    global network_data, flow_data
    if network_data is None or flow_data is None:
        return jsonify({'error': 'Both network and flow data must be uploaded'})

    # Ensure network_data has the correct structure
    if "network" not in network_data or "flows" not in flow_data:
        return jsonify({'error': 'Invalid network or flow data'})

    # Extract nodes and edges from network_data
    topology = network_data.get("network", {}).get("topology", {})
    nodes = topology.get("nodes", [])
    edges = topology.get("edges", [])

    # Combine network and flow data into flow_request_data
    flow_request_data = {
        "network": {
            "topology": {
                "nodes": nodes,
                "edges": edges
            }
        },
        "flows": flow_data["flows"]
    }

    # Save combined data to flow_request.json
    with open('flow_request.json', 'w') as file:
        json.dump(flow_request_data, file, indent=4)  # Pretty-print for readability

    return jsonify({'message': 'Combined data saved to flow_request.json'})

@app.route('/execute_gcl_controller', methods=['GET'])
def execute_gcl_controller():
    try:
        result = subprocess.run(['python3', 'gcl_controller.py'], capture_output=True, text=True)
        output = result.stdout
        return output
    except Exception as e:
        return str(e)

if __name__ == '__main__':
    app.run(debug=True)

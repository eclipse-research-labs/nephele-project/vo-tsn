import requests
import json



def make_api_request(url, headers, data):
    response = requests.get(url, headers=headers, json=data)
    if response.status_code != 200:
        print(f"Error: API request failed with status code {response.status_code}")
        exit()
    return response.json()

def main():
    # Make GET request to get flow_request.json
    combine_url = 'http://localhost:5000/combine_data'
    flow_request_response = requests.get(combine_url)
    if flow_request_response.status_code == 200:
        flow_request_data = flow_request_response.json()
        #print("Flow Request Data:", flow_request_data)
        # Use flow_request_data to make API request
        api_url = "http://localhost:8080/schedule/taprio"
        headers = {"Content-Type": "application/json"}
        with open ('flow_request.json', 'r') as file:
            request_data = json.load(file)

        data = make_api_request(api_url, headers, request_data)
        print("API Response:" , data)
        #api_response = data
        #print(data)
        print("Flows:")
        for flow in data.get("flows", []):
            print(f"Flow ID: {flow['flowID']}, Starts: {flow['starts']}, Arrives: {flow['arrives']}")

        return data
    else:
        print("Error: Failed to retrieve flow_request.json")
    return None
api_response = main()


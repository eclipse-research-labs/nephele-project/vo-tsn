# Running tests

In the case that the vo-tsn runs in the local host, simply run 

```runTests``` 

to initiate the tests. 

In the case the vo-tsn runs *remotely*, the simply provide the nesessary host information as a argument to the run tests:

```runTests <host:port>``` 




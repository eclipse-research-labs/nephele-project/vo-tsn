

%%% TSN Scheduler
 
%%% Parameters
%%% Sync error micro secs (uc)
delta(0).

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Utility Predicates for network data.
%%% link/4
%%% link(Src,Dest,Dely,Cap)
%%% Bidirectional link information.
link(Src,Dest,PropDelay,Cap):-
   lowlink(Src,Dest,PropDelay,Cap).
link(Src,Dest,PropDelay,Cap):-
    lowlink(Dest,Src,PropDelay,Cap).

%%% farbic_delay/2
%%% Computes the fabric delay
%%% If its a router, then there is a fabric delay, if not, then 
%%% its a talker, thus fabric delay = 0. 
fabric_delay(Src,Delay):-
    router(Src,Delay),!.
fabric_delay(_,0).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Top Level Predicate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% tsn_schedule/3
%%% tsn_schedule(Offsets, VarSelection, ValueSelection)
%%% Top level scheduling predicate (Offsets), 

tsn_schedule(Offsets,Heuristic,ValueSelection):-
    findall(F,flow(F,_,_,_),Flows),
    findall(R,router(R,_),Routers),
    compute_hyper_period(HP),
    collect_offsets(Flows,Offsets,Primary,Queues,HP),
    impose_router_cons(Routers,Offsets),
    %write('Searching...'),nl,
    %length(Offsets,Len),
    %write(['Vars::',Len]),nl,
    %!,
    %length(Primary,Len2),
    %write(['Primary Vars::',Len2]),nl,
    !,
    split_talker_router_offsets(Primary,TalkerOffsets, RouterOffsets),
%% Labeling queues    
    search(Queues,0,input_order,indomain_min,complete,[]),
%% labeling offsets   
%% We label routers first, since they present points 
    search(RouterOffsets,2,Heuristic,ValueSelection,complete,[]),
%% When it comes to talkers, we would like the latest time they start.    
    search(TalkerOffsets,2,Heuristic,indomain_max,complete,[]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Labeling Predicates

split_talker_router_offsets(Primary,TalkerOffsets, RouterOffsets):-
    net_talkers(Talkers),
    split_t_r_offsets(Talkers,Primary,TalkerOffsets,RouterOffsets).

%%% Auxiliary
split_t_r_offsets([],RestPrimary,[],RestPrimary).
split_t_r_offsets([Talker|Talkers],Primary,
                  [off(Talker,Off,Trans,Flow,Q)|TalkerOffsets],
                  RouterOffsets):-
    delete(off(Talker,Off,Trans,Flow,Q),Primary,RestPrimary),
    split_t_r_offsets(Talkers,RestPrimary,TalkerOffsets, RouterOffsets).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Collecting Queue Variables
%%% collect_offsets/5
%%% collect_offsets(Flows, Offsets, PrimaryOffsets, Queues, HyperPeriod)
%%% Succeeds if Offsets is the list of all Offsets, of flows 
%%% appearing in the list Flows, over the specific hyperperiod.

collect_offsets([],[],[],[],_). 
collect_offsets([Flow|Flows],Total_Offs,Primary,[Queue|RestQueues],HP):-
    aflow(Flow,PrimaryFlowOffsets,Queue), 
    flow(Flow,_,_,P),
% How many cycles (-1 since we have the first cycle. )
    Times is (HP // P),
    expand_offsets_to_hp(Times,P,PrimaryFlowOffsets, Offsets),
    collect_offsets(Flows,RestOffsets,RestPrimary,RestQueues,HP), 
    append(Offsets,RestOffsets, Total_Offs),
    append(PrimaryFlowOffsets,RestPrimary,Primary).

%%% expand_offsets_to_hp/4
%%% expand_offsets_to_hp(N,Period,PrimaryFlowOffsets, FlowOffsets)
%%% Expands the PrimaryOffsets (i.e. Offsets of Cycle A) to FlowOffsets the set 
%%% of all offesets of packets appeating in the hyperperiod, where N is the number of cycles 
%%% Period the period of the current flow.

expand_offsets_to_hp(1,_,PrimaryFlowOffsets,PrimaryFlowOffsets):-!.
expand_offsets_to_hp(N,Period,PrimaryFlowOffsets, FlowOffsets):-
    N > 1, 
    NN is N - 1 , %%% reducing NN before call
    copy_offsets(NN,Period,PrimaryFlowOffsets,CycleOffsets),
    expand_offsets_to_hp(NN, Period, PrimaryFlowOffsets, RestCycleOffsets), 
    append(RestCycleOffsets, CycleOffsets, FlowOffsets).     


%%% copy_offsets/4
%%% copy_offsets(N,Period,PrimaryOffsets,CycleOffsets)
%%% Given the number N of the cycle of period Period, and the PrimaryOffsets (i.e. Offsets of Cycle A)
%%% succeeds of CycleOffsets are the set of all offesets of packets appeating in the HyperPeriod.
copy_offsets(_,_P,[],[]).
copy_offsets(N,Period,[off(Router,Off,Trans,Flow,Q) | PrimaryFlowOffsets],[off(Router,CycleOff,Trans,Flow,Q)|CycleOffsets]):-
    CycleOff #= N * Period + Off, 
    copy_offsets(N,Period,PrimaryFlowOffsets,CycleOffsets).


%%% aflow/3
%%% aflow(Num,Offsets,Queue)
%%% Computes the primaryCycleOffset of the flow with id Num, 
%%% and succeeds when Offsets is the list of primaryCycleOffsets for
%%% that flow. 
%%% Computes the constraints of all offsets for a sigle flow in its period
%%% (see predicate flow above) on each of the visited nodes (routers) in the path.
/* Offsets are in the form of off(Node, Offset, TransDuration,Flow, link(Node,Dest,Queue)) where 
  - Node is the node in the network topology
  - Offset is the time point after 0 (start of schedule) that the transmission of the packet should start
  - TransDuration is the transmission duration (i.e. how long would the packet need to arrive to the other end)
  - Flow id
  - link(Node,Dest,Queue)  The outgoing link and the queue.
*/
aflow(FlowNum, Offsets,Queue):-
    flow(FlowNum,Path, _PacketSize,Period),
    offsets(Path,Offsets,FlowNum,Period),
% Introducing Relative Deadline
    flow_deadline(FlowNum,Talker,Listener,RelativeDeadline),
    member(off(Talker,Start,_,_,_),Offsets),
    member(off(Listener,End,_,_,_),Offsets),
    End - Start #=< RelativeDeadline,
% Ordering   
    Queue #::[1..7],
    ordering_constraint_per_flow(Offsets,FlowNum,Queue). 

%%% offsets/4
%%% offsets(Path, Offsets, Flow, Period)
%%% offsets succeeds of the list Offsets contains the functional terms defining scheduling information 
%%% of the packets in each network device in the path, i.e. define the flow decision vars and a free var for the 
%%% duration transmission on a link.
offsets([Listener],[off(Listener,Arrival,0,Flow,_)],Flow,Period):-
    Arrival #:: [0..Period].
offsets([Start|Rest],[off(Start,Offset,_Trans,Flow,_)|RestOffsets],Flow,Period):-
     Offset #:: [0..Period], 
     offsets(Rest,RestOffsets,Flow,Period). 

%%% ordering_constraint_per_flow/3
%%% ordering_constraint_per_flow(OffsetList,FlowNum,Queue)
%%% Imposes all delays on the links of the path, taking into account 
%%% - Fabric delay, propagation delay and transmission delay.
%%% Also imsposes that a flow follows the same queue in every router, however, this can be changed. 
%% Base case

%%% Added Delta as an extended duration

ordering_constraint_per_flow([  off(Src,Off,Trans,F,link(Src,Listener,Q)),
                                off(Listener,Arrival,0,F,link(Listener,Listener,Q))],
                                FlowNum,Q):-
      flow(FlowNum,_,Size,Period), 
      link(Src,Listener,PropDelay,Cap),
      %fabric_delay(Listener,FDelay),    
      delta(Delta),  
      Trans is 0 + PropDelay + integer(ceiling(Size / Cap)) + Delta,
      Arrival #= Off + Trans, 
      Arrival #=< Period. 

%% Intemediate link case      
ordering_constraint_per_flow([  off(Src,Off1,Trans,F,link(Src,Src2,Q)),
                                off(Src2,Off2,Trans2,F,link(Src2,Dest,Q))|Rest],
                                FlowNum,Q):-
      flow(FlowNum,_,Size,_), 
      link(Src,Src2,PropDelay,Cap),
      fabric_delay(Src2,FDelay), 
      delta(Delta),     
      Trans is PropDelay + integer(ceiling(Size / Cap)) + Delta,
% Ordering Cons: the next offset must be later 
      Off1 + FDelay + Trans  #=< Off2, 
      ordering_constraint_per_flow([off(Src2,Off2,Trans2,F,link(Src2,Dest,Q))|Rest],FlowNum,Q). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Routers and Associated constraints
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Top level to impose router constraints. 
impose_router_cons([],_Offsets).
impose_router_cons([Router|Routers],Offsets):-
    router_link_constraint(Router,Offsets),
    impose_router_cons(Routers,Offsets).

%%% router_link_constraint/2
%%% router_link_constraint(RouterID, Offsets)
%%% Ensures that links are used for the trans of a single packet.
%%% Ensures that packets arrive in the same order.
router_link_constraint(Router, Offsets):-
    routerFlowsInSameLink(Router,ListofListFlows),
    impose_per_link_dis(Router,ListofListFlows,Offsets).

%%%impose_per_link_dis/3.
%%% impose_per_link_dis(Router,ListofListofFlows,Offsets)
%%% Ensures that packets on the same link do not overlap, by imposing the 
%%% disjunctive constraint, considering that the link is a resource. 
 impose_per_link_dis(_,[],_).
 impose_per_link_dis(Router,[Flows|Rest],Offsets):-   
    collect_router_offsets(Router,Flows,Offsets,Offs,Trans),
% One flow can use the link
    disjunctive(Offs,Trans),
    fifo_order_router(Router,Flows,Offsets),
% Arriving at Queues.    
    impose_per_link_dis(Router,Rest,Offsets). 

%%%    
%%% Collect the offsets of the flows based on the routerID.
collect_router_offsets(_Router,_Flows,[],[],[]).   

collect_router_offsets(Router,Flows,[off(Router,Off,Trans,F,_Link)|Offsets],[Off|Offs],[Trans|RestTrans]):-
    member(F,Flows),
%    write(['flows:',Flows,Router,F,Link,Off]),nl,
    !, 
    collect_router_offsets(Router,Flows,Offsets,Offs,RestTrans).

collect_router_offsets(Router,Flows,[_|Offsets],Offs,RestTrans):-
    collect_router_offsets(Router,Flows,Offsets,Offs,RestTrans).

%%% routerFlowsInSameLink(Router,FlowsList)
%%% Computes the flows that pass from the router, for each link. 
%%% FlowsList is the list of lists of flows, where each inner list contains 
%%% the flows that pass from the same link (regardless of the queue to which the are assigned)
routerFlowsInSameLink(Router,FlowsList):-
     findall(Dest, (link(Router, Dest, _68, _69)
% DELETE we were missing listener desinations,router(Dest, _79)
                   ), DestList),
     findall(Flows,(member(Dest,DestList), 
                    findall(F,uses_link(Router,F,Dest),Flows), Flows \= [] 
                    ),
                     FlowsList).
%%% uses_link/3
%%% uses_link(Router,Flow,Dest)
%%% link (Router,Dest) is used by the flow.
uses_link(Router,Flow,Dest):-
    flow(Flow,Path,_,_),
    member(Router,Path), 
    once(append(_,[Router,Dest|_],Path)).

%%% Flows is a list of flows that share a egress link.
%%% fifo_order_router/3
fifo_order_router(_,[_],_):-!.
fifo_order_router(Router,[F1|Flows],Offsets):-
    fifo_order_router_flow(Router,F1,Flows,Offsets),
    fifo_order_router(Router,Flows,Offsets).

%%% fifo_order_router_flow/4
fifo_order_router_flow(_,_,[],_).
fifo_order_router_flow(Router,F1,[F2|Flows],Offsets):-
    ensure_packet_arrival(Router,F1,F2,Offsets),
    fifo_order_router_flow(Router,F1,Flows,Offsets).

%%% ensure_packet_arrival
ensure_packet_arrival(Router,F1,F2, Offsets):-
    member(off(Router,Off1,_,F1,link(Router,D,Q1)),Offsets),
    member(off(Router,Off2,_,F2,link(Router,D,Q2)),Offsets),
    getArrivalOffset(F1,Router,_PrevRouter,Arr1,Offsets),
    getArrivalOffset(F2,Router,_PrevRouter2,Arr2,Offsets),
%DBG    write([F1,Q1,F2,Q2,Router]), nl, 
    fifo_ordering_constraint(Off1,Arr1,Q1,Off2,Arr2,Q2).


getArrivalOffset(Flow,Router,PrevRouter,Arrival,Offsets):-
    flow(Flow,Path,_,_),
    append(_,[PrevRouter,Router|_],Path),
    member(off(PrevRouter,Offset,Trans,Flow,_),Offsets),
% There is only one
    !, 
    Arrival #= Offset+Trans.

getArrivalOffset(Flow,Router,_,_,_):-
    write(['Something went wrong in arrival offsets in flow - router',Flow,Router]),
    nl.


%%% Ensure FIFO Ordering.
fifo_ordering_constraint(Off1,Arr1,Q1,Off2,Arr2,Q2):- 
    (Q1 #= Q2) => ( 
     (Off1 #< Off2 => Arr1 #< Arr2) and 
     (Off1 #> Off2 => Arr1 #> Arr2) and 
     (Arr1 #< Arr2 => Off1 #< Off2) and 
     (Arr1 #> Arr2 => Off1 #> Off2)) 
     or Q1 #\= Q2.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Utilities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing HyperPeriod of Flows
compute_hyper_period(HP):-
    findall(P,flow(_,_,_,P),Periods),
    hyperPeriod(Periods,HP).

%%% HyperPeriod of a list of periods
hyperPeriod([],1).
hyperPeriod([P|PeriodList], HyperP):-
    hyperPeriod(PeriodList,HyperRest),
    lcm(P,HyperRest,HyperP).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Managing the network Information Base
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% net_talkers/1
%%% net_talker(Talkers)
%%% Succeeds if Talkers is the list of talkers in the net topology.   
net_talkers(Talkers):-
    setof(T,
        Flow^Deadline^Listener^flow_deadline(Flow,T,Listener,Deadline),Talkers).

%%% net_listeners/1
%%% net_listeners(Listeners)
%%% Succeeds if Listeners is the list of listeners in the net topology.   
net_listeners(Listeners):-
    setof(L,
        Flow^Deadline^Talker^flow_deadline(Flow,Talker,L,Deadline),Listeners).

%%% net_routers(Routers)
%%% Succeeds if Routers is the list of Routers in the topology
net_routers(Routers):-
    findall(R,router(R,_),Routers). 

%%% recursively succeeds listing all devices (talkers, listeners)
net_device(Device):-
    net_talkers(T),
    net_listeners(L),
    append(T,L,Devices),
    !, 
    member(Device,Devices).

%%% Just for debug

write_lst(List):-
    member(X,List),
    write(X),
    nl, fail.

write_lst(_).    

%%% Top level file


:-use_module('http_server_v11/http_server_v11.pl').
%%% Load all files at start/avoid error.json 
:-compile('http_method').
:-compile('tsn_main').
:-compile('tsn_json_interface').
:-compile('tsn_errors.ecl').

%%% Not much here... kust run the service on 8080.
run_scheduler:-
    http_server(8080).


# The ECLiPSe Prolog TSN Scheduler

An Eclipse Prolog implementation of the TSN scheduler for the Nephele project. 

The main objective of the scheduler is to determine a feasible scheduling pattern for incoming flows with respect to their specified requirements, such as size, period, deadlines, etc.


Scheduler is based on constraint programming, since the latter provides:
- flexibility in terms of problem modeling, and easy in meeting diverse future constraints, 
- robust constraint propagation techniques for search space reduction,
- effective problem agnostic heuristic search. 


The TSN queue scheduling problem can be considered as a classic job-shop scheduling problem, where each flow transmission considered as a task, consisting from as many transmission operations as the nodes it traverses along the path. 

Additional constraints ensure that packets do not overlap during transmission over links and arrive in the correct order in an ingress port (FIFO queues).

Modeling the above uses, apart from constraint reasoning on relations over variable expressions, global constraints, such are the disjunctive scheduling constraint, that fit the problem perfectly. The current implementation is based on the ECLiPSe constraint logic programming system (see link below). 

# Running the scheduler locally

In order to run the scheduler locally, you need to have installed The ECLiPSe Constraint Programming System (ECLiPSe CLP), which can be downloaded from https://eclipseclp.org/. 

Once ECLiPSe CLP has been installed, simply compile (load) in the Prolog interpreter the scheduler.ecl file, and type the query 

```run_scheduler```.

This will start a http service that accepts JSON requests on port 8080. 

Command line execution of the above (once eclipse is installed)

 ``` eclipse -f scheduler.ecl -e run_scheduler ```

Request Examples can be found in the folder jsonRequests and are explained later in this document. 

# Building the docker image of the Scheduler (only)
As usual, run 

``` docker build -t <your image name> . ```

The Dockerfile will automatically download the ECLiPSe runtime engine.

and then start the container 

``` docker run -p 8080:8080 <your image name> ```

to start the service. 

# Using the TSN Scheduler

## Forming a Request for a Schedulde
The current implementation, supports TSN schedule generation in the form of a http JSON requests, that describes the topology and flow requirements. This JSON file has two properties defined: ```network``` and ```flows```. 
The network property contains the topology in a form of a graph, i.e. a list of nodes objects and a list of edge objects. 

A **node** object:
- of type "endpoint", representing a talker or a listener (in TSN terminology), *or*
- of type "switch", represeting a TSN switch, in which case the processing delay or fabric delay in micro seconds (integer), should also be defined for that node.

All nodes objects must have a unique `id` (string).

An **edge** object, represents a link between two nodes, declared in its property `nodes`, and its transmission delay in micro-seconds (uc) in the property `delay` (integer) and its speed in Bits/uc (property `speed`).

For instance, the following defines a very simple topology, with two switches and two endpoints: 
```json
"network": {
    "topology": {
      "nodes": [
        { "id": "n1", "type": "endpoint" },
        { "id": "n2", "type": "switch","processingDelay": 1},
        { "id":"hub", "type":"switch", "processingDelay": 2},
        { "id": "n3", "type": "endpoint" }
      ],
      "edges": [
        {
          "link": "l1",
          "nodes": ["n1", "n2"],
          "delay": 1,
          "speed": 100
        },
        {
          "link": "l2",
          "nodes": ["n2", "hub"],
          "delay": 1,
          "speed": 100
        }, 
        {
          "link": "l3",
          "nodes": ["hub", "n3"],
          "delay": 1,
          "speed": 100
        }
      ]
    }
  }, 
  ```

The `flows` property contains the specifiation of the list of flows that are to be scheduled. Each flow, has a unique id (string), a relative deadline in micro seconds (integer), the packet size in Bits (integer), and the flows period in micro seconds (integer). Finally, the `path` property is the path of the flow in the form of a list of nodes, that have been defined previously in the network property, and has to be valid, i.e. must represent a valid path in the topology defined above. For example:   

```json
 "flows": [
    {
      "flowId": "flow1",
      "deadline": 800,
      "packetSize": 1500,
      "period": 800,
      "path": ["n1", "n2","hub","n3"]
    }]
```

## Invoking the Scheduler

The scheduler can generate output, either targeting TAPRIO, or the OMNET++ simulation environment. 

### TAPRIO
Interacting with the scheduler obtaining a solution tailored towards TAPRIO, is invoked via the API path:

```
GET http://<host>:<port>/schedule/taprio
```

with payload the JSON request file, described above. 

For instance, assume that the scheduler is running in the local host, on port 8080 and the JSON file that contains the request is "flow_request.jso", the correspoding curl command is:
```
curl -v -X GET -H "Content-Type: application/json" --data @flow_request.json http://localhost:8080/schedule/taprio
``` 

In this case, the answer is in the form of JSON, contains scheduling entries for each switch, in a form that can be easily translated to TAPRIO bash shell scripts.

```json
{
    "type": "taprio",
    "switches": [
        {
            "switch": "n2",
            "interfaces": [
                {
                    "src": "n2",
                    "dest": "hub",
                    "schedEntries": [
                        {
                            "schedEntry": 1,
                            "gatemask": "01",
                            "interval": 17000
                        },
                        {
                            "schedEntry": 2,
                            "gatemask": "02",
                            "interval": 29000
                        },
                        {
                            "schedEntry": 3,
                            "gatemask": "01",
                            "interval": 754000
                        }
                    ]
                }
            ]
        },
        {
            "switch": "hub",
            "interfaces": [
                {
                    "src": "hub",
                    "dest": "n3",
                    "schedEntries": [
                        {
                            "schedEntry": 1,
                            "gatemask": "01",
                            "interval": 35000
                        },
                        {
                            "schedEntry": 2,
                            "gatemask": "02",
                            "interval": 29000
                        },
                        {
                            "schedEntry": 3,
                            "gatemask": "01",
                            "interval": 736000
                        }
                    ]
                }
            ]
        }
    ],
    "flows": [
        {
            "flowID": "n1",
            "starts": 0,
            "arrives": 51
        },
        {
            "flowID": "n1",
            "starts": 4,
            "arrives": 64
        }
    ]
}
```

### OMNET++
Interacting with the scheduler obtaining a solution tailored towards OMNET++, is invoked via the API path:

```
GET http://<host>:<port>//schedule/omnet
```

with payload the JSON request file, described above. 

For Instance, using curl:
```
curl -v -X GET -H "Content-Type: application/json" --data @flow_request.json http://localhost:8080/schedule/omnet
``` 

### No Solution
In the case that a solution to the request does not exists, then the scheduler will report back the following:

```json
{
 "code":201,
 "message":"Solution not found to the Request."
 }
```
Not that this is not considered as an error (see next section), but as a valid answer to the request.

## Errors
There are multiple errors that are traced by the scheduler. The following a list of those errors. 

### Invalid json request
In the case the the json request does not follow the standard json syntax, the following will be reported back:
``` json
{"code":401,"message":"Invalid json format received."}
```

### Invalid json scheduler request
In the case that the request does not follow the syntax mentioned above, the following is reported back:
``` json
{"code":402,"message":"Invalid json scheduler request received."}
```

### Network topology and flow specification errors

Although a request can be syntactically correct, stil the topology defined or the specification of the flows might have a number of errors.
The following is a list of such errors:

In the case that the request contains multiple definitions of the same id for a router or a flow the following will be reported.
```json
{
 "code":403,
 "message":"Invalid Network descrition - router_id tsn_b1 is defined multiple times."
}
```
In the case that the request contains a specification of a topology that has errors, e.g. missing links, the following will be reported.
```json
{
 "code":403,
 "message":"Invalid Network descrition - Nodes r1 and tsn_b2 in flow flow1 do not have a link."
}
```

All errors with code 403, have an message that describes the error.


## Examples

The JSON file, along with curl requests in order to test the scheduler can be found in the [jsonRequests](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/tree/main/jsonRequests?ref_type=heads) folder.

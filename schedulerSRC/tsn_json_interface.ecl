:-module(tsn_json_interface).

:- export parse_json_request/2.
:- export encode_json/4.

:-lib(json).
:-lib(fromonto).

%%% top level
%%% Top level predicate to extract JSON info from request.
%%% parse_json_request/2
%%% parse_json_request(JSONString,Terms)
%%% Throws errors thus should be executed in a catch.
parse_json_request(JSONString,Terms):-
     json_read(input, JsonTerm, [names_as_atoms]) from_string JSONString,
     write('Parsing String OK'),nl,
     (parse_json_tsn_description(JsonTerm,Terms) -> true; throw(invalid_json_request)),
     !,
     write('JSON Conversion to Terms Generated'),nl.

%%% parse_json_tsn_description/2 
%%% parse_json_tsn_description(JsonTerm,Terms)
%%% Parses a Json Prolog Term as converted by the json lib predicate 
%%% to a list of terms required for the scheduler.
parse_json_tsn_description({JsonList},Terms):-
    extract_info(JsonList,ListTerms),
    flatten(ListTerms,StringTerms),
    findall(T,(member(X,StringTerms),flat_string_term(X,T)),Terms).

%%% extract_info/1
%%% extract_info(JsonList)
%%% Extract (recursively) information on all list items appearing 
%%% in the description. Succeeds if description is correct. 
extract_info([],[]).
extract_info([Item|Rest],[ItemTerm|RestItems]):-
      parse_json_item(Item,ItemTerm),
      extract_info(Rest,RestItems).

%%% Top json obcjects
parse_json_item(network:{Net},NetTerms):-
    parse_net_item(Net,NetTerms).

parse_json_item(flows:Flows,FlowTerms):-
    parse_flow_item(Flows,FlowTerms).

%%% parsing the list network items
parse_net_item([],[]):-!.

parse_net_item(nodes:ListNodes,NodeItems):-
    parse_node_items(ListNodes,NodeItems).

parse_net_item(edges:ListEdges,EdgeItems):-
    parse_edge_items(ListEdges,EdgeItems).

parse_net_item(topology:{ListNodes},NodeItems):-
    parse_net_item(ListNodes,NodeItems).

parse_net_item([A|Rest],[Item|RestItems]):-
    parse_net_item(A,Item),
    parse_net_item(Rest,RestItems).    

%%% parsing the list node items
%%% Node Info and related.
parse_node_items([],[]).
parse_node_items([{NodeDesc}|RestNodes],[endpoint(Name)|RestNodesTerms]):-
    member(type:"endpoint",NodeDesc),
    member(id:Name,NodeDesc),
    !,
    add_data(endpoint(Name)),
    parse_node_items(RestNodes,RestNodesTerms).
  
parse_node_items([{NodeDesc}|RestNodes],[router(NodeName,Delay)|RestNodesTerms]):-
    member(type:"switch",NodeDesc),
    member(id:NodeName,NodeDesc),
    member(processingDelay:Delay,NodeDesc),
    !,
    add_data(router(NodeName,Delay)),
    parse_node_items(RestNodes,RestNodesTerms).

%%%% parsing Edge Items creting the topology.
parse_edge_items([],[]).    
parse_edge_items([{LinkDesc}|RestNodes],[lowlink(Src,Dest,PropagationDelay,Speed)|RestEdgeItems]):-
    member(link:_Name,LinkDesc),
    member(nodes:[Src,Dest],LinkDesc),
    member(delay:PropagationDelay,LinkDesc),
    member(speed:Speed,LinkDesc),
    !,
    add_data(lowlink(Src,Dest,PropagationDelay,Speed)),
    parse_edge_items(RestNodes,RestEdgeItems).


%%%% Parsing flow items 
parse_flow_item([],[]):-!.

parse_flow_item({FlowItem},[flow(Flow,Path,Size,Period),flow_deadline(Flow,Src,Dst,DeadLine)]):-
   member(flowId:Flow,FlowItem),
   member(deadline:DeadLine,FlowItem),
   member(packetSize:Size,FlowItem),
   member(period:Period,FlowItem),
   member(path:Path,FlowItem),
   add_data(flow(Flow,Path,Size,Period)),
   append([Src|_],[Dst],Path),
   add_data(flow_deadline(Flow,Src,Dst,DeadLine)).

parse_flow_item([A|Rest],[FlowItem|RestItems]):-
    parse_flow_item(A,FlowItem),
    parse_flow_item(Rest,RestItems).    



%%% add_data/1
%%% add_data(Term)
%%% adds data from Json Description where needed (modularity)
% No Debug
add_data(_):-!.
/* To be used later
add_data(Term):-
    write('From add data '),
    flat_string_term(Term,FlatTerm),
    writeq(FlatTerm),nl. 
*/

%%% Flat Strings to Terms.
flat_string_term(Term,FlatTerm):-
    Term =.. [Func|Args],
    findall(FT,(member(T,Args),flat(T,FT)),FlatArgs),
    FlatTerm =..[Func|FlatArgs].

%%% Flat Strings, numbers, Lists.
flat([],[]).
flat(T,FT):-string(T), !, atom_string(FT,T).
flat(T,T):-number(T),!.
flat([T|Rest],[FT|RestFT]):-
    flat(T,FT), 
    flat(Rest,RestFT).    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Prolog to JSON
%%% type taprio/omnet
%%% The following code accepts a list of Prolog terms and converts them
%%% to a json prolog terms, o be handled by the Json library.

%%% encode_json/4
%%% encode_json(Type,AnswerTerms,Flows,Answer)
%%% Encodes to a Answer a JsonString that is the answer to the request. 
%%% Type can be either taprio or omnet, depending on the target tsn platform
%%% Answer also contains information regarding the scheduled flows. 
encode_json(taprio,AnswerTerms,Flows,Answer):-
   taprio_json(AnswerTerms,JSonStructure),
   flows_json(Flows,FlowsJson),
   json_write(output,{type:taprio,switches:JSonStructure, flows:FlowsJson},[indent(0)]) onto_string Answer.


encode_json(omnet,AnswerTerms,Flows,Answer):-
   omnet_json(AnswerTerms,JSonStructure),
   flows_json(Flows,FlowsJson),
   json_write(output,{type:omnet,switches:JSonStructure, flows:FlowsJson},[indent(0)]) onto_string Answer.
 

%%% parsing terms to Json
taprio_json([],[]).
taprio_json([(Switch,Links)|Rest],[{switch:Switch, interfaces:LinksJSON}|RestJson]):-
    taprio_links(Links,LinksJSON),
    taprio_json(Rest,RestJson).

taprio_links([],[]).
taprio_links([(Src,Dest,Schedule)|Rest],[{src:Src,dest:Dest,schedEntries:JsonSchedule}|RestLinks]):-
%%findall({gatemask:GateMask, interval:Interval},member((GateMask,Interval),Schedule), JsonSchedule), 
    taprio_sched_entry(1,Schedule,JsonSchedule),
    taprio_links(Rest,RestLinks).

taprio_sched_entry(_,[],[]).
taprio_sched_entry(N,[(GateMask,Interval)|Rest],[{schedEntry:N,gatemask:GateMask, interval:Interval}|RestEntries]):-
    NN is N +1,
    taprio_sched_entry(NN,Rest,RestEntries).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

omnet_json([],[]).
omnet_json([switch(Name,Links)|RestTerms],[{switch:Name, interfaces:LinksJSON}|RestJson]):-
       omnet_links(Links,LinksJSON), 
       omnet_json(RestTerms,RestJson).

 omnet_links([],[]).
 omnet_links([link(Src,Dest,Queues)|RestLinks],[{src:Src,dest:Dest,queues:Bands}|RestJson]):-
    omnet_queues(Queues,Bands),
    omnet_links(RestLinks,RestJson).

omnet_queues([],[]).
omnet_queues([[queue(N),offset(Off),band(List)]|RestTerms],[{queue:N,offset:Off,bands:FlatList}|RestJson]):-
    omnet_bands(List,FlatList),
    omnet_queues(RestTerms,RestJson).
    
omnet_bands([],[]).
omnet_bands([(On,Off)|Rest],[On,Off|RestFlat]):-
    omnet_bands(Rest,RestFlat).


%%% Parsing to Json Prolog terms information regarding flows.
flows_json([],[]).
flows_json([flow(ID,starts(S,_),_,_,arrives(_,at(Arrival,_)))| RestFlows],
           [{flowID:ID,starts:S,arrives:Arrival} |RestJson]):-
    flows_json(RestFlows, RestJson).

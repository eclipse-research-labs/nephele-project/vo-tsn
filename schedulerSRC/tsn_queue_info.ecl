%%% Prolog Code dealing with Queue Information. 

%%% Comment IS: NEEDS cleanup

%%% usage
%%% tsn_schedule(S,input_order,indomain_min), 
%%% !, member(D,[r1,r2,tsn_b1,tsn_b2]),
%%% gen_GCL(D,S,T), gen_taprio(T,TAP), write(TAP), nl, fail.

%%% Computes and creates information for use with TapRio
%%% Used for manual testing. 
taprio_schedule(Taprio_GCL,Flow_info):-
    tsn_schedule(Schedule,input_order,indomain_min), 
    !,
    net_routers(Routers),        
    findall(Tap, (
        member(R,Routers),
        gen_GCL(R,Schedule,taprio,Table),
        gen_taprio(Table,Tap) ),
        Taprio_GCL),
     !, 
     flow_info(Schedule,Flow_info). 


%%% Computes and creates information for use with Omnetpp
omnet_schedule(Bands,Flow_info):-
    tsn_schedule(Schedule,input_order,indomain_min),
    !,
    net_routers(Routers), 
    findall(Band, (
          member(Device,Routers),
          gen_GCL(Device,Schedule,simple,Table), 
          gen_omnet(Table,Band) ),
          Bands),

    !,flow_info(Schedule,Flow_info).        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% queue_info_extract/4
%%% queue_info_extract(Type,Schedule,Taprio_GCL,Flow_info)
%%% Extracts from a schedule Schedule, information specific to taprio or 
%%% omnet scheduling queues. 
queue_info_extract(taprio,Schedule,Taprio_GCL,Flow_info):-
    net_routers(Routers),        
    findall(Tap, (
        member(R,Routers),
        gen_GCL(R,Schedule,taprio,Table),
        gen_taprio(Table,Tap) ),
        Taprio_GCL),
     !, 
     flow_info(Schedule,Flow_info). 


queue_info_extract(omnet,Schedule,Bands,Flow_info):-
    net_routers(Routers), 
    findall(Band, (
          member(Device,Routers),
          gen_GCL(Device,Schedule,simple,Table), 
          gen_omnet(Table,Band) ),
          Bands),
    !,
    flow_info(Schedule,Flow_info).        




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Collecting flow info.
flow_info(Schedule,Flow_info):-
    findall(F,flow(F,_,_,_), Flows),
    collect_flow_info(Flows,Schedule,Flow_info).

collect_flow_info([],_,[]).
collect_flow_info([Flow|RestFlows],Schedule,
                  [flow(Talker,starts(StartsAt,us),size(Size,bits),period(Period,us),arrives(Listener,at(ArrivesAt,us)))|RestF]):-
    flow(Flow,[Talker|Path],Size,Period),
    append(_,[Listener],Path),
    member(off(Talker,StartsAt,_,Flow,_),Schedule),
    member(off(Listener,ArrivesAt,_,Flow,_),Schedule),
    !,
    collect_flow_info(RestFlows,Schedule,RestF).

%%% MODE is taprio for taprio configuration as above
%%%   simple: a simpler version with the(timestap,Duration,queue) to be open
%%%   raw: raw version with (timestamp, duration, open queue)

gen_GCL(Device,Offsets,MODE,table(Device,GCL)):-
    generate_router_GCL(Device,Offsets,LinkOffs),
    build_device_GCL(MODE,LinkOffs,GCL).

%%% Builds the GCL for the device under consideration
build_device_GCL(_,[],[]). 
build_device_GCL(MODE, [Offs|LinkOffs],[linkGCL(Src,Dest,GCL)|RestGCLs]):-
    process_linkOffs(0,Offs,GCLAll,Src,Dest),
    post_process(MODE,GCLAll,GCL),
    !, 
    build_device_GCL(MODE,LinkOffs,RestGCLs).


post_process(taprio, GCLAll,GCL):-
   !,post_process_TR(GCLAll,GCL).

post_process(simple, GCLAll,GCL):-
   !,post_process(GCLAll,GCL).

post_process(raw, GCL,GCL).


% fills in the "blanks" Blacks are when the best effort queue is activated.
% maybe a bit "heavy" on Src, Dest however it provides sanity checks.
% last
process_linkOffs(CurrentTime,[],[],_,_):- 
      compute_hyper_period(HP), 
      CurrentTime is HP,!.

process_linkOffs(CurrentTime,[],[(CurrentTime,Dur,0)],_,_):- 
      compute_hyper_period(HP), 
      CurrentTime < HP, !,
      Dur is HP-CurrentTime.

process_linkOffs(CurrentTime,[],[],_,_):- 
      write(['ERROR Hypertime exceeded in schedule. ', CurrentTime]), 
      nl,!,fail.


process_linkOffs(CurrentTime,[gcl(Src,CurrentTime,Dur,link(Src,Dest,Q),_)|Rest],
                             [(CurrentTime,Dur,Q)|RestGCL],
                             Src,Dest):-
    NextCurrentTime is CurrentTime + Dur,
    process_linkOffs(NextCurrentTime,Rest,RestGCL,Src,Dest).

% there is a gap
process_linkOffs(CurrentTime,[gcl(Src,Offset,Dur,link(Src,Dest,Q),_)|Rest],[(CurrentTime,FOR,0)|RestGCL],Src,Dest):-
    CurrentTime < Offset, 
    FOR is Offset - CurrentTime,
    process_linkOffs(Offset,[gcl(Src,Offset,Dur,link(Src,Dest,Q),_)|Rest],RestGCL,Src,Dest).

%%% Needs to be changed.


%%% NOTE: This is similar to the above, so at some point some 
%%% rengineering is required. 

post_process_TR([(_Time,Dur,Q)],[(Q,Dur)]).
%% after change
%post_process_TR([(Time,Dur,Q)],[(Q,Dur),(0,LastD)]):-
%    Q \= 0,
%    compute_hyper_period(HP), LastD is HP - (Time + Dur).

post_process_TR([(Time1,Dur1,Q),(Time2,Dur2,Q)|Rest],RestEntry):-
    Time2 =:= Time1 + Dur1, %% they are consequtive
    DurTotal is Dur1 + Dur2, 
    post_process_TR([(Time1,DurTotal,Q)|Rest],RestEntry).

post_process_TR([(_Time,Dur,Q1),(Time2,Dur2,Q2)|Rest],[(Q1,Dur)|RestEntry]):-
   Q1 \= Q2, 
   post_process_TR([(Time2,Dur2,Q2)|Rest],RestEntry).




%%% post_process/2
%post_process([(_Time,_Dur,0)],[]):-!.

% base case
%post_process([(Time,Dur,Q)],[(Time,Q),(Last,0)]):-Q \= 0, Last is Time + Dur.

post_process([(Time,Dur,Q)],[(Time,Dur,Q)]). 

post_process([(Time1,Dur1,Q),(Time2,Dur2,Q)|Rest],RestEntry):-
    !, 
    Time2 =:= Time1 + Dur1, %% they are consequtive
    DurTotal is Dur1 + Dur2, 
    post_process([(Time1,DurTotal,Q)|Rest],RestEntry).

post_process([(Time,Dur,Q1),(Time2,Dur2,Q2)|Rest],[(Time,Dur,Q1)|RestEntry]):-
   Q1 \= Q2, 
    post_process([(Time2,Dur2,Q2)|Rest],RestEntry).

%%% Generates the GCL for a router on  a specific egress link.
%%% on backtracking it provides all egress links.
generate_router_GCL(Router,Offsets, TotalLinkOffsets):-
    findall(off(Router,Off,Trans,F,Q),
            member(off(Router,Off,Trans,F,Q),Offsets), 
            OffsetList),
    %% findall((Router,Dest),(link(Router,Dest,_,_), router(Dest,_)), Links),
    findall((Router,Dest),(link(Router,Dest,_,_)), Links),
    findall(LinkOffsets, (
        member((Router,Dest),Links),
        generate_GCL_port(Router,Dest,OffsetList,LinkOffsets)), 
        TotalLinkOffsets).
    %%%write(LinkOffsets),nl.    



generate_GCL_port(Router,Dest,Offsets,QueueOffsetList):-
        setof(gcl(Router,Off,Trans,link(Router,Dest,Q),F),
            Trans^F^member(off(Router,Off,Trans,F,link(Router,Dest,Q)),Offsets), 
            QueueOffsetList).


%%% Output Format
%%% TAP RIO OUTPUT
%%% mapping queue representation in code with TAPRIO
%%% mappingQueues/2
%%% mappingQueues(Queue,TAPRIORep)
mappingQueues(0,'01').
mappingQueues(1,'02').
mappingQueues(2,'04').
mappingQueues(3,'08').
mappingQueues(4,'16').
mappingQueues(5,'32').
mappingQueues(6,'64').
mappingQueues(7,'128').



gen_taprio(table(Device,Egress_List),(Device,TAPRIO_Rep)):-
    map_internal_GCL_TAPRIO(Egress_List,TAPRIO_Rep).


map_internal_GCL_TAPRIO([],[]).
map_internal_GCL_TAPRIO([linkGCL(SRC,DEST,SCH)|RestGCL],
                        [(SRC,DEST,TAPRIO_S)|REST_TAPRIO]):-
            findall((TAP_Queue,TAP_Dur),
                    (member((Queue,Duration),SCH),
                     mappingQueues(Queue,TAP_Queue),
                     TAP_Dur is Duration * 1000),
                     TAPRIO_S),
            map_internal_GCL_TAPRIO(RestGCL,REST_TAPRIO). 


%%%% OMNET++ Output

%%% gen_omnet
gen_omnet(table(Device,Egress_List), switch(Device,OMNET_Rep)):-
    map_internal_GCL_onmet(Egress_List,OMNET_Rep).

%%% perfroms the mapping
 map_internal_GCL_onmet([],[]).   
 map_internal_GCL_onmet([linkGCL(Src,Dest,Sched)|Egress_List],[link(Src,Dest,OMNET_links)|OMNET_Rep]):-
% collects queues
    setof(Q, Time^Dur^member((Time,Dur,Q),Sched),QueueList),
    build_omnet(QueueList,Sched,OMNET_links),
    map_internal_GCL_onmet(Egress_List,OMNET_Rep).

%% 
build_omnet([],_Sched,[]).
build_omnet([Q|QueueList],Sched,[FixedBand|RestBands]):-
      findall((Time,Dur,Q), member((Time,Dur,Q), Sched), QueueSchedule),
      create_band(QueueSchedule,Band),
      fix_band(Band,FixedBand),
      build_omnet(QueueList,Sched,RestBands).

create_band([(TimeStamp,Dur,Q)|Rest],[queue(Q),offset(TSNeg),band(Band)]):-
        TSNeg is -TimeStamp,
        create_band([(TimeStamp,Dur,Q)|Rest], TimeStamp, Band). 

%%% Last
create_band([(TimeStamp,Dur,_)], OffSet, [(Dur,Closing)]):-
                   compute_hyper_period(HP), 
                   Closing is  HP + OffSet -(TimeStamp + Dur).

create_band([(Time1,Dur1,Q),(Time2,Dur2,Q)|RestSchedules], OffSet, [(Dur1,Closing)|Rest]):-
                   Closing is  Time2 - (Time1 + Dur1),
                   create_band([(Time2,Dur2,Q)|RestSchedules], OffSet,Rest).

%%% fix_band/2
%%% fix_band(Band,FixedBand)
%%% Fixing the bad to avoid 0 at the end. 
%% Only bands with >1 are to be fixed
fix_band([queue(Q),offset(OffSet),band([(Up,Down)|Band])],
         [queue(Q),offset(NewOffSet),band(FixedBand)]  ):-
    append(MiddleBand,[(UpLast,0)],Band),
    !,
    NewUp is Up + UpLast, 
    NewOffSet is OffSet + UpLast, 
    append([(NewUp,Down)],MiddleBand,FixedBand).

% No change if last band is non zero or has a single entry.
fix_band(Band,Band).

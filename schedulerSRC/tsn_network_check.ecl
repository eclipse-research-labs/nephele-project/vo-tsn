%%% Code to check the network description of 
%%% the received json request. 

%% DELETE BEFORE UPLOADING
/*
link(Src,Dest,PropDelay,Cap):-
   lowlink(Src,Dest,PropDelay,Cap).
link(Src,Dest,PropDelay,Cap):-
    lowlink(Dest,Src,PropDelay,Cap).
*/ 


%%% top level goal to check the network request.
check_network_in_request:-
     check_unique_routers(_Routers),
     check_unique_flow_id(FlowIds),
     check_paths(FlowIds),
     check_flow_deadlines(FlowIds).

%%% 
check_unique_routers(RouterList):-
    findall(RouterID,router(RouterID,_),RouterList),
    check_unique(RouterList,router_id).

%%% 
check_unique_flow_id(FlowIds):-
    findall(F,flow(F,_,_,_),FlowIds),
    check_unique(FlowIds,flow_id). 

%%% 
check_paths([]).

check_paths([Flow|FlowIds]):-
    flow(Flow,[Listener|Path],_,_),
    check_single_path([Listener|Path],Flow),
    check_paths(FlowIds).

%%%
check_single_path([_Last],_Flow).

check_single_path([Node,Next|Path],Flow):-
    link(Node,Next,_,_),
    !,
    check_single_path([Next|Path],Flow).

check_single_path([Node,Next|_],Flow):-
    sprintf(Cause,"Nodes %w and %w in flow %w do not have a link.",[Node,Next,Flow]), 
    throw(invalid_network_description(Cause)).    


%%% 
check_flow_deadlines([]).
check_flow_deadlines([Flow|FlowIds]):-
    flow_deadline(Flow,Talker,Listener,Deadline),
    flow(Flow,Path,_,Period),
    check_deadline_period(Flow,Period,Deadline),
    check_talker_listener(Flow,Talker,Listener,Path),
    check_flow_deadlines(FlowIds).

%%%
check_deadline_period(_Flow,Period,Deadline):-
    Period >= Deadline,!.

check_deadline_period(Flow,Period,Deadline):-
    sprintf(Cause,"In flow %w period %w is less than deadline %w",[Flow,Period,Deadline]),
    throw(invalid_network_description(Cause)).    

%%%
check_talker_listener(_Flow,Talker,Listener,[Talker|Path]):-
      append(_,[Listener], Path),!.

check_talker_listener(Flow,Talker,Listener,_):-
    sprintf(Cause,"In flow %w specification there is a problem with either talker %w or listener %w",[Flow,Talker,Listener]),
    throw(invalid_network_description(Cause)).    
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Check Unique for different types and raise an exception.
check_unique([],_Type).
check_unique([X|Rest],Type):-
    not(member(X,Rest)),
    !,
    check_unique(Rest,Type).
check_unique([X|_],Type):-
    sprintf(Cause,"%w %w is defined multiple times.",[Type,X]), 
    throw(invalid_network_description(Cause)).    




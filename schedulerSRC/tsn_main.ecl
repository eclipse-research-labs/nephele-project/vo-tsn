:-module(tsn_main).

%% :-export answer_json_request/3. 
:-export answer_json_request/4. 

%%% EClipse Constraint Code 
:-lib(ic).
:-lib(ic_global).
:-lib(branch_and_bound).
:-lib(ic_edge_finder).

%%% Http Json API
%:-use_module('http_server_v11/http_server_v11').
%:-use_module(http_method).
:-use_module(tsn_json_interface).
:-use_module(tsn_errors).

%%%% Network Defintion is in files
:-dynamic router/2.
:-dynamic lowlink/4.
:-dynamic flow/4.
:-dynamic flow_deadline/4.

%%% clear_net/0
%%% Clears network definition of dynamic predicates
%%% representing the topology. 
clear_net:-
    retractall(router(_,_)),
    retractall(lowlink(_,_,_,_)),
    retractall(flow(_,_,_,_)),
    retractall(flow_deadline(_,_,_,_)).

%%% Asserting a list of terms
assert_list([]).
assert_list([NetTerm|Rest]):-
    assertz(NetTerm),
    assert_list(Rest).

:-include('tsn_queue_info.ecl').
:-include('tsn_sched.ecl').
:-include('tsn_network_check.ecl').
%:-include('tsn_errors.ecl'). 


%%% %%% answer_json_request/4
%%% answer_json_request(Type,JsonString,Code, Answer)
%%% Answers json request comming from http_method/6.
%%% Formats answer in json with respect to the type of request (taprio/omnet)

answer_json_request(Type,JsonString,Code, Answer):-
    catch( (answer_json_request(Type,JsonString,Answer),Code = 200),
        ERROR,
        sched_manage_errors(ERROR,Code,Answer)).
    

%%% answer_json_request/3
%%% answer_json_request(Type,JsonString,Answer)
%%% Answers json request comming from http_method/6.
%%% Formats answer in json with respect to the type of request (taprio/omnet)
answer_json_request(Type,JsonString,Answer):-
    parse_json_request(JsonString,NetworkInfoTerms),
    clear_net,
    assert_list(NetworkInfoTerms),
    write('Net Info Asserted!'),nl,
% Checking whether the network description is consistent    
    check_network_in_request,
% compute_tsn_schedule(Type,AnswerTerms,FlowInfo),
    (tsn_schedule(Schedule,input_order,indomain_min) -> true ; throw(solution_not_found)),
    write('Schedule Found.'),nl,
    queue_info_extract(Type,Schedule,AnswerTerms,FlowInfo),
    encode_json(Type,AnswerTerms,FlowInfo,Answer),
    write('Answer Encoded.'),nl,
    true.

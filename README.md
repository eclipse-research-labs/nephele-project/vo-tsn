# VO TSN Control Plane


# Overall Description
This repo presents the prototype implementation of a TAS-compliant scheduler to
address the specific needs of IoT-VO communication in terms
of bounded latency and rapid schedule computation, every time
the IoT-VO association has changed or when a VO migrates
to another host or edge cloud. The TSN scheduling problem
at hand has been formulated and tackled using constraint
programming.

![alt text](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/raw/main/documents/control_plane.png?ref_type=heads)


This repo includes the following implementations the ```flow_and_path module``` the ```gcl_controller module``` and the ```Scheduler module``` the first two are in the /src folder and the scheduler is in /schedulerSRC folder.   

The repo also includes a Netconf Server implementation [Netconf](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/tree/main/netconfServer?ref_type=heads).


Scheduler Service: This service is responsible for the generation of the schedules. It exposes port 8080. A health check is configured to ensure the service is running correctly. The health check performs a curl request to http://localhost:8080/schedule/health every second, with a timeout of 1 second, and will retry up to 10 times.

This is work in progress.

# Running the scheduler container

Further information and instructions regarding how to run the TSN Scheduler standalone can be found [here](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/vo-tsn/-/tree/main/schedulerSRC?ref_type=heads)

# References

Please cite the following papers, if you use the code:
- G. Papathanail, L. Mamatas, T. Theodorou, I Sakellariou, P. Papadimitriou, N. Filinis, D. Spatharakis, E. Fotopoulou, A. Zafeiropoulos, S. Papavassiliou, A Virtual Object Stack for IoT-Enabled Applications Across the Compute Continuum, IEEE/ACM UCC CEICO 2023.

- G. Papathanail, I. Sakellariou, L. Mamatas, and P. Papadimitriou, Dynamic Schedule Computation for Time-Aware Shaper in Converged IoT-Cloud Environments, IEEE ICIN 2024

- G. Papathanail, L. Mamatas, and P. Papadimitriou, Towards the Integration of TAPRIO-Based Scheduling with Centralized TSN Control, IFIP/IEEE Networking TENSOR 2023


